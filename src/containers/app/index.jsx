import React from "react";
import { Container, Row, Col } from 'react-grid-system';
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'

// Style
import style from './style.css';

// Components
import Header from '../../components/header';
import Loading from '../../components/loading';
import Button from '../../components/button';
import Coupons from '../coupons';
import Summary from '../summary';
import Confirm from './confirm';
import Cancel from './cancel';

// Services
import { getCheckoutData } from '../../services/checkoutService';

const swal = withReactContent(Swal);

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      "product": {
        "title": "",
        "price": 0,
        "image": ""
      },
      "checkout": {
        "shippingPrice": 0,
        "availableCoupons": [],
        "totalPrice": 0
      },
      discount: 0
    }

    this.handleOptionChage = this.handleOptionChage.bind(this);
    this.handleConfirm = this.handleConfirm.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
  }

  async getData() {
    const { params } = this.props.match;
    const response = await getCheckoutData(params.checkout);

    if (response.status > 299) {
      return;
    }

    this.setState({
      loading: false,
      product: response.data.product,
      checkout: response.data.checkout
    })
  }

  handleOptionChage(value) {
    this.setState({
      discount: parseFloat(value)
    })
  }

  handleConfirm() {
    swal.fire({
      html: <Confirm />,
      showConfirmButton: false,
    })
  }

  handleCancel() {
    swal.fire({
      html: <Cancel />,
      showConfirmButton: false,
    })
  }

  componentWillMount() {
    this.getData();
  }

  render() {
    const {product, checkout, discount, loading} = this.state;

    return (
      <Container fluid>
        <Row>
          <Col xs={12}>
            <Header />
          </Col>
        </Row>
        <Row>
          <Col xs={12} sm={6} className={style.productImg}>
            {loading ? <Loading /> : null}
            <div className={style.box}>
              <img src={product.image} alt={product.title}/>
            </div>
          </Col>
          <Col xs={12} sm={6}>
            <Row>
              <Col xs={12}>
                <div className={style.box}>
                  <Coupons list={checkout.availableCoupons} onChangeOption={this.handleOptionChage} />
                </div>
              </Col>
            </Row>
            <Row>
              <Col xs={12}>
                <div className={style.box}>
                  <Summary
                    value={product.price}
                    discount={discount}
                    shipping={checkout.shippingPrice}
                  />
                </div>
              </Col>
            </Row>
            <Row>
              <Col xs={12}>
                <div className={style.box}>
                  <div className={style.buttons}>
                    <Button cancel width="calc(50% - 2px)" onClick={this.handleCancel}>
                      cancel
                    </Button>
                    <Button width="calc(50% - 2px)" onClick={this.handleConfirm}>
                      confirm
                    </Button>
                  </div>
                </div>
              </Col>
            </Row>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default App;
