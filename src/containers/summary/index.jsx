import React from "react";
import PropTypes from "prop-types";
import style from "./style.css";

class Summary extends React.Component {
  constructor() {
    super();
  }

  formatMoney(value) {
    return parseFloat(value).toFixed(2).replace('.', ',');
  }

  render() {
    const { value, discount, shipping } = this.props;
    return (
      <div>
        <h1 className={style.title}>resumo</h1>
        <div className={style.item}>
          <span>valor original</span>
          <span className={style.value}>R$ {this.formatMoney(value)}</span>
        </div>
        <div className={style.item}>
          <span>cupom</span>
          <span className={style.discount}>-R$ {this.formatMoney(discount)}</span>
        </div>
        <div className={style.item}>
          <span>frete</span>
          <span className={style.value}>R$ {this.formatMoney(shipping)}</span>
        </div>
        <div className={style.item}>
          <span>total</span>
          <span className={style.value}>R$ {this.formatMoney(value + shipping - discount)}</span>
        </div>
      </div>
    );
  }
}

Summary.propTypes = {
  value: PropTypes.number.isRequired,
  discount: PropTypes.number.isRequired,
  shipping: PropTypes.number.isRequired,
}

export default Summary;
