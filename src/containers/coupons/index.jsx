import React from "react";
import PropTypes from "prop-types";
import style from "./style.css";

class Coupons extends React.Component {
  constructor() {
    super();
  }

  handleChange = event => {
    const { onChangeOption } = this.props;

    onChangeOption(event.target.value);
  }

  renderOptions = () => {
    const { list } = this.props;

    return (
      <div>
        <div className={style.coupon}>
          <label>
            <span className={style.check}></span>
            <input type="radio" onChange={this.handleChange} name="coupon" value={0}/>
            <span className={style.option}>não usar cupom</span>
          </label>
        </div>
        { list.length > 0 &&
          list.map((coupon, id) => (
            <div key={id} className={style.coupon}>
              <label key={id}>
                <input key={id} type="radio" onChange={this.handleChange } name="coupon" value={coupon.discount}/>
                <div className={style.option}>
                  <span>{coupon.title}</span>
                  <span className={style.value}>-R$ {coupon.discount.toFixed(2).replace('.', ',')}</span>
                </div>
              </label>
            </div>
          ))
        }
      </div>
    )
  }

  render() {
    return (
      <div>
        <h1 className={style.title}>cupons</h1>
        {this.renderOptions()}
      </div>
    );
  }
}

Coupons.propTypes = {
  list: PropTypes.array.isRequired,
  onChangeOption: PropTypes.func.isRequired
}

export default Coupons;
