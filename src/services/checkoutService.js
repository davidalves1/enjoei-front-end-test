import axios from 'axios';

const API_URL = 'http://localhost:3000/api';

export const getCheckoutData = async (id) => {
  try {
    const response = await axios.get(`${API_URL}/checkouts/${id}`)

    return response;
  } catch (error) {
    if (error.response) {
      return {
        status: error.response.status,
        data: {}
      }
    }

    return {
      status: 500,
      data: {}
    }
  }
}
