import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Switch, Route, Link, browserHistory } from 'react-router-dom'
import Checkout from './containers/app';

// Não façam isso em casa
class Start extends React.Component {
  render() {
    return (
      <div style={{textAlign: 'center', margin: '1rem'}}>
        <Link to="/produto/1321/checkout/6544" style={{textDecoration: 'none'}}>
          <h1 style={{color: '#5b5855'}}>Meu, clica aqui e olha que tela de checkout lindona...</h1>
        </Link>
      </div>
    )
  }
}

ReactDOM.render((
  <BrowserRouter history={browserHistory}>
    <Switch>
      <Route path="/" exact={true} component={Start} />
      <Route path='/produto/:product/checkout/:checkout' component={Checkout} />
      <Route path="**" component={Start} />
    </Switch>
  </BrowserRouter>
), document.querySelector('#app'));
