import React from "react";
import PropTypes from "prop-types";

import style from "./style.css";

class Button extends React.Component {
  constructor() {
    super();
  }

  handleClick = () => {
    const { onClick } = this.props;

    onClick();
  }

  render() {
    const { children, cancel, width } = this.props;
    const classButton = cancel ? style.cancel : style.default;

    return (
      <button
        className={classButton}
        style={{width: width ? width: '100%'}}
        onClick={this.handleClick}
      >
        {children}
      </button>
    );
  }
}

Button.propTypes = {
  cancel: PropTypes.bool,
  width: PropTypes.string,
  onClick: PropTypes.func,
}

export default Button;
