import React from 'react';
import style from './style.css';

class Loading extends React.Component {
  render() {
    return (
      <div className={style.loader}></div>
    );
  }
}

export default Loading;
